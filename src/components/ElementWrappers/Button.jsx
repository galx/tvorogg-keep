import React from 'react';
import PropTypes from 'prop-types';

const defaultProps = {
  disabled: false,
  icon: false,
};

const propTypes = {
  ariaLabel: PropTypes.string,
  className: PropTypes.string,
  ariaPressed: PropTypes.bool,
  onInteraction: PropTypes.func,
  disabled: PropTypes.bool,
  icon: PropTypes.element,
};

/**
 * @ACCESIBILITY
 * @EVENT_HANDLING
 * @FP
 */
const handleKeyDown = onInteraction => (event) => {
  if (event.keyCode === 13 || event.keyCode === 32) {
    event.preventDefault();
    alert("dsd");
    onInteraction.onInteraction();
  }
};


const Button = (props) => {
  const dynamicAttributes = {};
  if (props.icon) {
    dynamicAttributes.dangerouslySetInnerHTML = { __html: props.icon };
  }
  return (
    <div
      role="button"
      className={props.className}
      disabled={props.disabled}
      aria-label={props.ariaLabel}
      aria-pressed={props.ariaPressed}
      tabIndex="0"
      onClick={props.onInteraction}
      onKeyDown={handleKeyDown(props.onInteraction)}
      {...dynamicAttributes}
    >
      {props.children}
    </div>
  );
};

Button.propTypes = propTypes;
Button.defaultProps = defaultProps;

export default Button;
