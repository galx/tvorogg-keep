import { actionType } from '../actions/';

const defaultNoteState = {
  title: '',
  note: '',
  selected: false,
  pinned: false,
};

const initialState = {
  abc: {
    id: 'abc',
    title: 'типа демка',
    note: 'Окей, творог. Это - новая модель организационной деятельности, которая представляет собой интересный эксперимент проверки новых предложений. С другой стороны консультация с широким активом представляет собой интересный эксперимент проверки новых предложений. Задача организации, в особенности же постоянный количественный рост и сфера нашей активности представляет собой интересный эксперимент проверки систем массового участия. Значимость этих проблем настолько очевидна, что новая модель организационной деятельности позволяет выполнять важные задания по разработке дальнейших направлений развития. С другой стороны реализация намеченных плановых заданий представляет собой интересный эксперимент проверки позиций, занимаемых участниками в отношении поставленных задач.',
    selected: false,
    pinned: false,
  },
};

/**
 * @REDUXING -- State Design
 */
function notes(state = initialState, action) {
  switch (action.type) {
    case actionType.ADD_NOTE:
      return {
        ...state,
        ...action.payload,
      };
    case actionType.SELECT_NOTE:
      return {
        ...state,
        [action.payload.id]: {
          ...state[action.payload.id],
          selected: !state[action.payload.id].selected,
        },
      };
    case actionType.PIN_NOTE:
      return {
        ...state,
        [action.payload.id]: {
          ...state[action.payload.id],
          pinned: !state[action.payload.id].pinned,
        },
      };
    case actionType.UPDATE_NOTE:
      return {
        ...state,
        [action.payload.id]: {
          ...state[action.payload.id],
          ...action.payload,
        },
      };
    default:
      return state;
  }
}

export { actionType, defaultNoteState, notes };

export default notes;
